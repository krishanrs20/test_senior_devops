def infraCreation = ""
def versionHistoryPath = ""
def authVersion = ""
def aiVersion = ""
def webVersion = ""
def insightVersion =""
def servicesVersion = ""
def functionsVersion = ""
def aiTaggingVersion = ""
def authJob = ""
def aiJob = ""
def webJob = ""
def insightJob = ""
def servicesJob = ""
def functionJob = ""
def aiTaggingJob = ""
def resetCallQueueJob = ""

pipeline {
  agent any
  stages {
  //  1. Send Deployment start to Slack  
    stage('Send Notification') {
      steps {
        slackSend (color: '#FFFF00', message: "STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
      }
    }

    stage('Checking Request') {
      steps {
        script{
          if(params.ENVIRONMENT != params.CONFIRM_ENV) {
            error("Aborting Job due to environment confirmation error")
          }
          else {
            echo "User request validated successful"
          }
        }
      }
    }

    stage('Read Version') {
      when {
        expression {
          return params.ACTION == 'create'
        }
      }
      steps {
        script{
          versionHistoryPath = "${env.PIPELINE_SCRIPTS_DIR}/viq.infra.orchestration/"
          if (params.AUTH_APP == true) {
            def authJsonContent = readJSON file: "${versionHistoryPath}/${params.ENVIRONMENT}/authentication.json"
            authVersion = authJsonContent.recordset[0].Version

            if (authVersion.count(".") == 2) {
              authJob = "viq.application.deployment"
            } else {
              authJob = "viq.hotfix.deployment"
            }
          }
          
          if (params.AI_APP == true) {
            def aiJsonContent = readJSON file: "${versionHistoryPath}/${params.ENVIRONMENT}/ai.json"
            aiVersion = aiJsonContent.recordset[0].Version

            if (aiVersion.count(".") == 2) {
              aiJob = "viq.application.deployment"
            } else {
              aiJob = "viq.hotfix.deployment"
            }            
          }

          if (params.WEB_APP == true) {
            def webJsonContent = readJSON file: "${versionHistoryPath}/${params.ENVIRONMENT}/web.json"
            webVersion = webJsonContent.recordset[0].Version

            if (webVersion.count(".") == 2) {
              webJob = "viq.application.deployment"
            } else {
              webJob = "viq.hotfix.deployment"
            }   
          }

          if (params.INSIGHT_APP == true) {
            def insightJsonContent = readJSON file: "${versionHistoryPath}/${params.ENVIRONMENT}/insight.json"
            insightVersion = insightJsonContent.recordset[0].Version

            if (insightVersion.count(".") == 2) {
              insightJob = "viq.application.deployment"
            } else {
              insightJob = "viq.hotfix.deployment"
            }   
          }

          if (params.API_APP == true) {
            def servicesJsonContent = readJSON file: "${versionHistoryPath}/${params.ENVIRONMENT}/services.json"
            servicesVersion = servicesJsonContent.recordset[0].Version

            if (servicesVersion.count(".") == 2) {
              servicesJob = "viq.application.deployment"
            } else {
              servicesJob = "viq.hotfix.deployment"
            }
          }

          if (params.FUNCTION_APP == true) {
            def functionsJsonContent = readJSON file: "${versionHistoryPath}/${params.ENVIRONMENT}/functions.json"
            functionsVersion = functionsJsonContent.recordset[0].Version

            if (functionsVersion.count(".") == 2) {
              functionJob = "viq.application.deployment"
            } else {
              functionJob = "viq.hotfix.deployment"
            }
          }

          if (params.AI_TAG_APP == true) {
            def aiTaggingJsonContent = readJSON file: "${versionHistoryPath}/${params.ENVIRONMENT}/viq.ai.tagging.json"
            aiTaggingVersion = aiTaggingJsonContent.recordset[0].Version

            if (aiTaggingVersion.count(".") == 2) {
              aiTaggingJob = "viq.application.deployment"
            } else {
              aiTaggingJob = "viq.hotfix.deployment"
            }
          }

          if (params.RESET_CALL_QUEUE_APP == true) {
            def resetCallQueueContent = readJSON file: "${versionHistoryPath}/${params.ENVIRONMENT}/viq.resetcallqueue.json"
            resetCallQueueVersion = resetCallQueueContent.recordset[0].Version

            if (resetCallQueueVersion.count(".") == 2) {
              resetCallQueueJob = "viq.application.deployment"
            } else {
              resetCallQueueJob = "viq.hotfix.deployment"
            }
          }
        }
      }
    }

    stage('Write Current Versions') {
      when {
        expression {
          return params.ACTION == 'destroy'
        }
      }
      steps {
        script {
          versionHistoryPath = "${env.PIPELINE_SCRIPTS_DIR}/viq.infra.orchestration/"
        }
        dir(path: "${versionHistoryPath}") {
          sh "mkdir -p ${params.ENVIRONMENT}"
          sh "curl -X GET  'http://xxxxxxxxxxxxxxxxxxxxxxxxx.azure.com:3000/api/environment/info?env=${params.ENVIRONMENT}&service=viq.authentication' -o ${params.ENVIRONMENT}/authentication.json"
          sh "curl -X GET  'http://xxxxxxxxxxxxxxxxxxxxxxxxx.azure.com:3000/api/environment/info?env=${params.ENVIRONMENT}&service=viq.ai' -o ${params.ENVIRONMENT}/ai.json"
          sh "curl -X GET  'http://xxxxxxxxxxxxxxxxxxxxxxxxx.azure.com:3000/api/environment/info?env=${params.ENVIRONMENT}&service=viq.web' -o ${params.ENVIRONMENT}/web.json"
          sh "curl -X GET  'http://xxxxxxxxxxxxxxxxxxxxxxxxx.azure.com:3000/api/environment/info?env=${params.ENVIRONMENT}&service=viq.services' -o ${params.ENVIRONMENT}/services.json"
          sh "curl -X GET  'http://xxxxxxxxxxxxxxxxxxxxxxxxx.azure.com:3000/api/environment/info?env=${params.ENVIRONMENT}&service=viq.insight' -o ${params.ENVIRONMENT}/insight.json"
          sh "curl -X GET  'http://xxxxxxxxxxxxxxxxxxxxxxxxx.azure.com:3000/api/environment/info?env=${params.ENVIRONMENT}&service=viq.functions' -o ${params.ENVIRONMENT}/functions.json"
          sh "curl -X GET  'http://xxxxxxxxxxxxxxxxxxxxxxxxx.azure.com:3000/api/environment/info?env=${params.ENVIRONMENT}&service=viq.ai.tagging' -o ${params.ENVIRONMENT}/viq.ai.tagging.json"
          sh "curl -X GET  'http://xxxxxxxxxxxxxxxxxxxxxxxxx.azure.com:3000/api/environment/info?env=${params.ENVIRONMENT}&service=viq.resetcallqueue' -o ${params.ENVIRONMENT}/viq.resetcallqueue.json"
        }
      }
    }

    stage('Infrastructure Creation / Destroy') {
      steps {
        script {
          if (params.ACTION == 'create') {
            infraCreation =  "true"
          }
          else {
            infraCreation = "false"
          }
          build job: 'viq.env.deployment', parameters: [
            string(name: 'PRODUCT_NAME', value: "viq_en"),
            string(name: 'ENVIRONMENT', value: params.ENVIRONMENT),
            string(name: 'CREATE', value: infraCreation),
            string(name: 'BOOLEAN_VALUE', value: infraCreation)
          ]
        }
      }
    }

    //4.2. Deploy All applications
    stage('Application Deployment') {
      when {
        expression {
          return params.ACTION == 'create'
        }
      }

      steps {
        script {
          if (params.WEB_APP == true) {
            build job: webJob, parameters: [
              string(name: 'PROJECT_NAME', value: "viq.web"),
              string(name: 'ENVIRONMENT', value: params.ENVIRONMENT),
              string(name: 'VERSION', value: webVersion)
            ]
          }

          if (params.AUTH_APP == true) {
            build job: authJob, parameters: [
              string(name: 'PROJECT_NAME', value: "viq.authentication"),
              string(name: 'ENVIRONMENT', value: params.ENVIRONMENT),
              string(name: 'VERSION', value: authVersion)
            ]
          }

          if (params.API_APP == true) {
            build job: servicesJob, parameters: [
              string(name: 'PROJECT_NAME', value: "viq.services"),
              string(name: 'ENVIRONMENT', value: params.ENVIRONMENT),
              string(name: 'VERSION', value: servicesVersion)
            ]
          }

          if (params.AI_APP == true) {
            build job: aiJob, parameters: [
              string(name: 'PROJECT_NAME', value: "viq.ai"),
              string(name: 'ENVIRONMENT', value: params.ENVIRONMENT),
              string(name: 'VERSION', value: aiVersion)
            ]
          }

          if(params.FUNCTION_APP == true) {
            build job: functionJob, parameters: [
              string(name: 'PROJECT_NAME', value: "viq.functions"),
              string(name: 'ENVIRONMENT', value: params.ENVIRONMENT),
              string(name: 'VERSION', value: functionsVersion)
            ]
          }

          if (params.INSIGHT_APP == true) {
            build job: insightJob, parameters: [
              string(name: 'PROJECT_NAME', value: "viq.insight"),
              string(name: 'ENVIRONMENT', value: params.ENVIRONMENT),
              string(name: 'VERSION', value: insightVersion)
            ]
          }

          if (params.AI_TAG_APP == true) {
            build job: aiTaggingJob, parameters: [
              string(name: 'PROJECT_NAME', value: "viq.ai.tagging"),
              string(name: 'ENVIRONMENT', value: params.ENVIRONMENT),
              string(name: 'VERSION', value: aiTaggingVersion)
            ]
          }

          if (params.RESET_CALL_QUEUE_APP == true) {
            build job: resetCallQueueJob, parameters: [
              string(name: 'PROJECT_NAME', value: "viq.resetcallqueue"),
              string(name: 'ENVIRONMENT', value: params.ENVIRONMENT),
              string(name: 'VERSION', value: resetCallQueueVersion)
            ]
          }
        }
      }
    }

    
    stage('Clean') {
      steps {
        cleanWs cleanWhenAborted: true, cleanWhenFailure: true, cleanWhenSuccess: true, notFailBuild: true
      }
    }
  }

 // 7. Send deployment status to Slack
  post {
    success {
      slackSend (color: '#00FF00', message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    }
    failure {
      slackSend (color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    }
    aborted {
      slackSend (color: '#FF0000', message: "ABORTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    }
  }
  parameters {
    booleanParam(defaultValue: false, name: 'WEB_APP', description: 'Select to deploy Web App Service')
    booleanParam(defaultValue: false, name: 'AUTH_APP', description: 'Select to deploy Authetication App Service')
    booleanParam(defaultValue: false, name: 'API_APP', description: 'Select to deploy API App Service')
    booleanParam(defaultValue: false, name: 'AI_APP', description: 'Select to deploy AI App Service')
    booleanParam(defaultValue: false, name: 'FUNCTION_APP', description: 'Select to deploy Function App Service')
    booleanParam(defaultValue: false, name: 'INSIGHT_APP', description: 'Select to deploy Insight App Service')
    booleanParam(defaultValue: false, name: 'AI_TAG_APP', description: 'Select to deploy AI Tag App Service')
    booleanParam(defaultValue: false, name: 'RESET_CALL_QUEUE_APP', description: 'Select to deploy Reset Call Queue App Service')


    choice(choices:['dv01','qa01', 'rnd01', 'st01', 'pr01', 'pr02', 'aeg'], description: 'Select environment', name: 'ENVIRONMENT')
    choice(choices:['create','destroy'], description: 'Select Action', name: 'ACTION')
    string(name: 'CONFIRM_ENV', description: 'Confirm the Environment. i.e: rnd01')
  }  
}

